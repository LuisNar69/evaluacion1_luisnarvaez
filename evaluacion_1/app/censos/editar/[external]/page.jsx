'use client';
import mensajes from "@/componentes/Mensajes";
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm } from 'react-hook-form';
import Link from "next/link";
import { useRouter } from 'next/navigation';
import { enviar, obtenerCensos, obtenerCursos, obtenerEscuelas } from "@/hooks/Conexion";
import { useEffect, useState } from "react";
import { getId, getToken } from "@/hooks/SessionUtilClient";

export default function Page({params}) {

  const router = useRouter();
  const { external } = params;
  const key = getToken();
  const externaluser = getId();



  const [escuelas, setEscuelas] = useState([]);
  const [llamada, setLlamada] = useState(false);
  const [cursos, setCursos] = useState([]);
  const [llamada2, setLlamada2] = useState(false);
  const [ninos, setNinos] = useState([]);
  const [llamada3, setLlamada3] = useState(false);


  //validaciones
  const validationShema = Yup.object().shape({
    peso: Yup.number().required('Ingrese el peso'),
    altura: Yup.number().required('ingrese la altura'),
    representante: Yup.string().required('Ingrese un representante'),
    actividades: Yup.string().required('ingrese actividades'),

    external_escuela: Yup.string().required('Seleccione una escuela'),
    external_curso: Yup.string().required('Seleccione un curso'),
  });

  const formOptions = { resolver: yupResolver(validationShema) };
  const { register, handleSubmit, setValue, formState, } = useForm(formOptions);
  const { errors } = formState;

  //Metodo para modificar censos
  const sendData = (data) => {
    var datos = {
      'weight': data.peso,
      'height': data.altura,
      'representative': data.representante,
      'activities': data.actividades,
      'external_course': data.external_curso,
      'external_school': data.external_escuela,
      'external': external,
      "resource": "updateCensus"
    };

    enviar('examen.php', datos, key).then((info) => {
      if (info.code !== 200) {
        mensajes("ninos no se pudo modificar", "Error", "error")
      } else {
        mensajes("Censo modificado correctamente", "Informacion", "success")
        router.push("/censos");
      }
    });
  };

  //llamar escuelas
  if (!llamada) {
    obtenerEscuelas('examen.php/?resource=school', key).then((info) => {
      if (info.code === 200) {
        setEscuelas(info.info);
        setLlamada(true);
      } else {
        mensajes("No se pudo cargar las Escuelas", "Error", "error");
      }
    });
  };

  //llamar cursos
  if (!llamada2) {
    obtenerCursos('examen.php/?resource=course', "").then((info) => {
      if (info.code === 200) {
        setCursos(info.info);
        setLlamada2(true);
      } else {
        mensajes("No se pudo cargar los cursos", "Error", "error");
      }
    });
  };

  if (!llamada3) {
    obtenerCensos('examen.php/?resource=census_children_login&external=' + externaluser, key).then((info) => {
      if (info.code === 200) {
        info.info.forEach(function (element) {
          if (element.extrenal_censo === external) {
            setValue('peso', element.peso);
            setValue('altura', element.talla);
            setValue('representante', element.representante);
            setValue('actividades', element.actividades);
            setValue('external_curso', element.external_curso);
            setValue('external_escuela', element.external_escuela);
          }
        })
        setLlamada3(true);
      } else {
        mensajes("No se pudo Listar los ninoss", "Error", "error");
      }
    });
  };



  return (
    <div className="wrapper" >
      <center>
        <br /><br />
        <div className="d-flex flex-column" style={{ width: 700 }}>
          <h5 className="title" style={{ color: "black", font: "bold" }}>Modificar CENSOS</h5>
          <br />

          <div className='container-fluid'>
            <form className="user" onSubmit={handleSubmit(sendData)}>


              <div className="row mb-4">
                <div className="col">
                  <input {...register('peso')} name="peso" id="peso" placeholder="Ingrese el peso" className={`form-control ${errors.peso ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.peso?.message}</div>
                </div>
                <div className="col">
                  <input {...register('altura')} name="altura" id="altura" placeholder="Ingrese la altura" className={`form-control ${errors.altura ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.altura?.message}</div>
                </div>
              </div>
              <div className="row mb-4">
                <div className="col">
                  <input {...register('representante')} name="representante" id="representante" placeholder="Ingrese un representate" className={`form-control ${errors.representante ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.representante?.message}</div>
                </div>
                <div className="col">
                  <input {...register('actividades')} name="actividades" id="actividades" placeholder="Ingrese las actividades" className={`form-control ${errors.actividades ? 'is-invalid' : ''}`} />
                  <div className='alert alert-danger invalid-feedback'>{errors.actividades?.message}</div>
                </div>
              </div>

              <div className="row mb-4">
                <div className="col">
                  <select {...register('external_escuela')} name="external_escuela" id="external_escuela" className={`form-control ${errors.external_escuela ? 'is-invalid' : ''}`}>
                    <option>Elija una escuela</option>
                    {escuelas.map((aux, i) => {
                      return (<option key={i} value={aux.external_id}>
                        {aux.nombre}
                      </option>)
                    })}
                  </select>
                  <div className='alert alert-danger invalid-feedback'>{errors.external_escuela?.message}</div>
                </div>
              </div>
              <div className="row mb-4">
                <div className="col">
                  <select {...register('external_curso')} name="external_curso" id="'external_curso'" className={`form-control ${errors.external_curso ? 'is-invalid' : ''}`}>
                    <option>Elija una Curso</option>
                    {cursos.map((aux, i) => {
                      return (<option key={i} value={aux.external_id}>
                        {aux.denominacion}
                      </option>)
                    })}
                  </select>
                  <div className='alert alert-danger invalid-feedback'>{errors.external_curso?.message}</div>
                </div>
              </div>


              <hr />
              <button type='submit' className="btn btn-success">GUARDAR</button>

            </form>
          </div>
          {<Link style={{ margin: "5px" }} href="/censos" className="btn btn-danger font-weight-bold">Regresar</Link>}
        </div>
      </center>
    </div>
  );
}

