'use client';
import mensajes from "@/componentes/Mensajes";
import Menu from "@/componentes/menu";
import { obtenerCensos } from "@/hooks/Conexion";
import { getId, getToken } from "@/hooks/SessionUtilClient";
import Link from "next/link";
import { useState } from "react";


export default function Page() {

    const key = getToken();
    const external = getId();

    const [ninos, setNinos] = useState([]);
    const [llamada, setLlamada] = useState(false);

    //llamar niños censados
    if (!llamada) {
        obtenerCensos('examen.php/?resource=census_children_login&external=' + external, key).then((info) => {
            if (info.code === 200) {
                setNinos(info.info);
                setLlamada(true);
            } else {
                mensajes("No se pudo Listar los documentos", "Error", "error");
            }
        });
    };


    return (

        <div className="row">
            <Menu></Menu>
            <h1 style={{ textAlign: "center" }}> Administrar Censos</h1>
            <div className="container-fluid">
                <div className="col-12 mb-4" style={{ alignItems: "center" }}>

                    <Link style={{ margin: "15px" }} href="/censos/nuevo" className="btn btn-success font-weight-bold">Registrar</Link>

                </div>
                <table className="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Nro</th>
                            <th>Niños</th>
                            <th>Edad</th>
                            <th>Talla</th>
                            <th>Peso</th>
                            <th>Representante</th>
                            <th>Escuela</th>
                            <th>Actividades</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {ninos.map((dato, i) => (
                            <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{dato.nombres}</td>
                                <td>{dato.edad}</td>
                                <td>{dato.talla}</td>
                                <td>{dato.peso}</td>
                                <td>{dato.representante}</td>
                                <td>{dato.escuela}</td>
                                <td>{dato.actividades}</td>
                                <td>
                
                                    {<Link style={{ margin: "5px" }} href={`/censos/editar/${dato.extrenal_censo}`} className="btn btn-warning font-weight-bold">Modificar</Link>}
                                </td>
                            </tr>
                        )
                        )}

                    </tbody>
                </table>

            </div>
        </div>
    );
}


