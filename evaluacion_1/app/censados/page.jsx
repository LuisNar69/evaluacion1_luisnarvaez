'use client';
import mensajes from "@/componentes/Mensajes";
import Menu from "@/componentes/menu";
import { obtenerCensados } from "@/hooks/Conexion";
import { borrarSesion, getId, getToken } from "@/hooks/SessionUtilClient";
import Link from "next/link";
import { useState } from "react";

export default function Page() {

    const key = getToken();
    const external = getId();

    const [niños, setNiños] = useState([]);
    const [llamada, setLlamada] = useState(false);

    //llamar niños censados
    if (!llamada) {
        obtenerCensados('examen.php/?resource=census_children', key).then((info) => {
            if (info.code === 200) {
                setNiños(info.info);
                setLlamada(true);
            } else {
                mensajes("No se pudo Listar los documentos", "Error", "error");
            }
        });
    };

    const salir = () => {
        borrarSesion();
    }

    return (

        <div className="row">
            <Menu></Menu>
            <h1 style={{ textAlign: "center" }}> Niños Censados</h1>
            <div className="container-fluid">
                <div className="col-12 mb-4" style={{ alignItems: "center" }}>

                </div>
                <table className="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Nro</th>
                            <th>Nombres</th>
                            <th>Edad</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        {niños.map((dato, i) => (
                            <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{dato.nombres}</td>
                                <td>{dato.edad}</td>
                                <td>{dato.isCensado}</td>
                            </tr>
                        )
                        )}

                    </tbody>
                </table>

            </div>
        </div>
    );
}


