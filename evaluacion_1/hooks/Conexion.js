let URL = 'https://computacion.unl.edu.ec/pdml/examen1/';

//devolver la url
export function url_api() {
    return URL;
}

//Censados
export async function obtenerCensados(recurso, key = "") {
    let headers = {};

    headers = {
        'TEST-KEY': key,
    };

    const response = await (await fetch(URL + recurso, {
        method: "GET",
        headers: headers,
        cache: 'no-store',
        // mode: 'no-cors',
    })).json();
    return response;
}

//Censos
export async function obtenerCensos(recurso, key = "") {
    let headers = {};

    headers = {
        'TEST-KEY': key,
    };

    const response = await (await fetch(URL + recurso, {
        method: "GET",
        headers: headers,
        cache: 'no-store',
        // mode: 'no-cors',
    })).json();
    return response;
}

//buscar  Censos
export async function Censo(recurso, key = "") {
    let headers = {};

    headers = {
        'TEST-KEY': key,
    };

    const response = await (await fetch(URL + recurso, {
        method: "GET",
        headers: headers,
        cache: 'no-store',
        // mode: 'no-cors',
    })).json();
    return response;
}


//Obtener Escuelas
export async function obtenerEscuelas(recurso, key) {
    let headers = {};

    headers = {
        'TEST-KEY': key,
    };

    const datos = await (await fetch(URL + recurso, {
        method: "GET",
        headers: headers,
        cache: 'no-store',
    })).json();
    return datos;
}

//Obtener Niños
export async function obtenerNinos(recurso, key) {
    let headers = {};

    headers = {
        'TEST-KEY': key,
    };

    const datos = await (await fetch(URL + recurso, {
        method: "GET",
        headers: headers,
        cache: 'no-store',
    })).json();
    return datos;
}

//Obtener Cursos
export async function obtenerCursos(recurso, key) {
    let headers = {};

    headers = {
        'TEST-KEY': key,
    };

    const datos = await (await fetch(URL + recurso, {
        method: "GET",
        headers: headers,
        cache: 'no-store',
    })).json();
    return datos;
}

//enviar peticion
export async function enviar(recurso, data, key = "") {
    let headers = {};

    if (key !== "") {
        headers = {
            'TEST-KEY': key
        };
    } else {
        headers = {

        };
    }

    const response = await fetch(URL + recurso, {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    });

    return await response.json();
}