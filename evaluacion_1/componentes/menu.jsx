'use client';
import { borrarSesion } from "@/hooks/SessionUtil";
import Link from "next/link";

export default function Menu() {

    const salir = () => {
        borrarSesion();
     }

    return (
        <nav className="navbar navbar-expand-lg bg-body-tertiary" data-bs-theme="dark">
            <div className="container-fluid">
                <h1 className="navbar-brand" >SISTEMA  SENSOS</h1>
                
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">

                        <li className="nav-item">
                        <Link className="nav-link active" aria-current="page" href={'/censos'}>Censos</Link>
                        </li>
                        <li className="nav-item">
                        <Link className="nav-link active" aria-current="page" href={'/censados'}>Censados</Link>
                        </li>

                        <li className="nav-item">
                            <a className="nav-link active" href="/inicio_sesion" onClick={salir}>
                                Cerrar Sesión
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
    );
}